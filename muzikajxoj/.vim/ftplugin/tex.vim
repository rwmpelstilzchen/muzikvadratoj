function! Lyoc()
	%s/r/w/g
	%s/c'/C/g
	%s/cis'/CIS/g
	%s/des'/DES/g
	%s/d'/D/g
	%s/dis'/DIS/g
	%s/ees'/EES/g
	%s/e'/E/g
	%s/\\\d//g
	%s/(\|)//g
	%s/\<\(\a\S*\)/\\\1/g
	%s/ //g
endfunction

function! Lyoc8()
	call Lyoc()
	%s/\<\(\a*\)8\>/\1/g
	%s/\<\(\a*\)4\./\1\\z\\z/g
	%s/\<\(\a*\)4/\1\\z/g
	%s/\<\(\a*\)2\./\1\\z\\z\\z\\z\\z/g
	%s/\<\(\a*\)2/\1\\z\\z\\z/g
	%s/\<\(\a*\)1\>/\1\\z\\z\\z\\z\\z\\z\\z/g
endfunction

function! Lyoc4()
	call Lyoc()
	%s/\<\(\a*\)4\>/\1/g
	%s/\<\(\a*\)2\./\1\\z\\z/g
	%s/\<\(\a*\)2/\1\\z/g
	%s/\<\(\a*\)1/\1\\z\\z\\z/g
endfunction


function! Lyocfix8()
	%s/\(\\\a*\)8\(\\\a*\)\./\\duo{\1}{\2}\\z/g
	%s/\(\\\a*\)\.\(\\\a*\)8/\1\\duo{\\z}{\2}/g
	%s/\(\\\a*\)8\(\\\a*\)8/\\duo{\1}{\2}/g
endfunction

function! Lyocfix16()
	%s/\(\\\a*\)\.\(\\\a*\)16/\1\\duo{\\z}{\2}/g
	%s/\(\\\a*\)16\(\\\a*\)\./\\duo{\1}{\2}\\z/g
	%s/\(\\\a*\)16\(\\\a*\)16/\\duo{\1}{\2}/g
endfunction

function! Takto()
	%s/\([^|]\)$/\1}{}/g
	%s/|$/}{\\x}/g
	%s/^\(.\)/\\takto{}{\1/g
endfunction
