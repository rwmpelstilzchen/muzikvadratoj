#!/usr/bin/python3

import sqlite3

conn = sqlite3.connect("../datumaro/datumaro.db")
conn.row_factory = sqlite3.Row

xstr = lambda s: s or ""


def depop(compositor):
    if compositor == "{\\popola}" or compositor == "עממי" or compositor == "{\\sennoma}" or compositor == "אלמוני/ת" or compositor == "{\\nekonata}" or compositor == "לא ידוע":
        return ""
    else:
        return compositor


def process_category(category):
    c = conn.cursor()
    for row in c.execute("SELECT * FROM muzikaĵoj\
                         WHERE (mk='vv') AND kategorio='" + category + "'\
                         ORDER BY `titolo-he`"):
        # print("*" + row['dosiernomo'] + "*")
        print("\muzikajxo" +
              "{" + row['dosiernomo'] + "}" +
              "{" + row['titolo-he'] + "}" +
              "{" + depop(row['komponisto-he']) + "}"
              "{" + row['titolo-eo'] + "}" +
              "{" + depop(row['komponisto-eo']) + "}" +
              "{" + xstr(row['titolo-xx']) + "}" +
              "{" + xstr(row['komponisto-xx']) + "}" +
              "{" + row['ikono'] + "}"
              )
    print()


c = conn.cursor()
for row in c.execute("SELECT kategorio, COUNT(*)\
                     FROM muzikaĵoj\
                     WHERE (mk = 'vv')\
                     GROUP BY kategorio\
                     ORDER BY COUNT(*) DESC\
                     "):
    if row[1] > 0:
        process_category(row[0])
