\eosection{Antaŭparolo}

Tiu ĉi libro naskiĝis el ĝia ĝemelo, «Okarineto», libro por lernigi ludi per etan blovinstrumenton nomata «Okarino». La formo de notskribado en «Okarineto» estas sama, kiel la ĉi tia: kvadratoj signifantaj sonojn (tie, en la kvadratoj aperas sĥemoj de okarino kun signoj pri la fermendaj truoj) kaj kvadratoj signifantaj daŭrigon aŭ maldaŭrigon de la antaŭa sono, kaj ĉiu kvadrato havas sama daŭro. Post kiam la libro eldoniĝis uneldone mi ekkonsciis, ke mi povas utiligi la fakto, ke la libro produktiĝas per komputilo, kaj modifi la komandoj produktantaj sĥemoj al komandoj produktantaj koloroj. Tiel, oni povas krei aŭtomate libron por lernigi ludi per klavarinstrumentoj se oni markas la klavojn per multkoloraj glumarkoj. El la ideo al la realigo la distanco estis principe teĥnika, kaj ne longa; jen la rezulto: \hl{Muzikoloroj}. La fakto, ke la muzikaĵoj en la libro havas limon de unu oktavo plus maĵora trito (el \textit{do} al \textit{mi} en la sekva oktavo), devenas de la registra limo de ses-trua okarino.

La \hl{kriterio} por elekti la muzikaĵoj estas simpla kaj subjektiva: muzikaĵoj, kiujn mi kaŭ mia idoj ŝatas.

La dispartigado al diversaj \hl{kategorioj} estas tre ĝenerala, kaj estas intencita esti helpo al direktado, ne kiel sistema klasifikado.



\eosection{Kio estas tabulaturo, kaj kiel oni legas ĝin?}

Ni konas skribado de muziko per la kutima moderna okcidentuma notskribo, signifante la tonalto per grafika alto kaj la tondaŭro per la formo de notoj. Tiu muzika notacio konvenas multajn instrumentojn (tiu estas malspecifa), kaj havas multajn teoriajn kaj realajn utilojn. En \hl{tabulaturo}, tamen, tio, kiu estast signifita, ne estas la tonalto, sed la maniero de sia farado per specifa instrumento. Tio faras la tabulaturon pli alirebla por junaj lernantoj.

La muzikaĵoj en tiu ĉi libro estas skribitaj je meniero, kiu konvenas \hl{klavarinstrumentoj kaj ksilofono}, se oni markas la klavojn per multkoloraj glumarkoj\footnote{\EO{Aŭ ciferhavaj glumarkoj; vidu la paĝon por «Muzikoloroj» en la retejo de la eldonejo: \url{http://xpr.digitalwords.net}}}. Ĉiu sono havas koloron; tre simpla.

Jen \hl{ĥromata gamo}\footnote{\EO{Ĉu vi scias, ke «ĥromata» devenas de la greka vorto \Gre{χρῶμα} «koloro»?}}. Gluu la glumarkojn, de la suplementa paĝo, sur la blankaj klavoj, el \emph{do} (C) dekstren (ĉu vi ne scias kie \emph{do} (C) estas, legu la artikolon «Klavaro (muziko)» ĉe Vikipedio); la nigraj klavoj estas markitaj en la libro per duonigita kvadrato, kaj ĝia du flankoj signifas la du blankaj klavoj apud ĝi.

\renewcommand{\enliniaskalaro}{0.35}
\begin{tabular}{cccccc}
	\enlinianotacio{\c} & \enlinianotacio{\cis} & \enlinianotacio{\d} & \enlinianotacio{\dis} & \enlinianotacio{\e} & \enlinianotacio{\f}\\
	\fbox{C} &
	C\symbolglyph{♯} / D\symbolglyph{♭} &
	\fbox{D} &
	D\symbolglyph{♯} / E\symbolglyph{♭} &
	\fbox{E} &
	\fbox{F}\vspace{2ex}\\
	\enlinianotacio{\fis} & \enlinianotacio{\g} & \enlinianotacio{\gis} & \enlinianotacio{\a} & \enlinianotacio{\ais} & \enlinianotacio{\b}\\
	F\symbolglyph{♯} / G\symbolglyph{♭} &
	\fbox{G} &
	G\symbolglyph{♯} / A\symbolglyph{♭} &
	\fbox{A} &
	A\symbolglyph{♯} / B\symbolglyph{♭} &
	\fbox{B}\vspace{2ex}\\
	\enlinianotacio{\C} & \enlinianotacio{\CIS} & \enlinianotacio{\D} & \enlinianotacio{\DIS} & \enlinianotacio{\E}\\
	\fbox{C} &
	C\symbolglyph{♯} / D\symbolglyph{♭} &
	\fbox{D} &
	D\symbolglyph{♯} / E\symbolglyph{♭} &
	\fbox{E}
\end{tabular}
\normigienlinianskalaron

La vario de melodioj, kiun oni povas ludi kun tiuj 17 sonoj, estas miriga.

\hl{Ritmo}. Ĉiuj kvadratoj havas la saman daŭron. \enlinianotacio{\z} enspacas unu ritman daŭron, \enlinianotacio{\mbox{\LR{\c\z}}} daŭras do duoblan daŭron kontraste al \enlinianotacio{\c}; \enlinianotacio{\w} signifas paŭzon. Se aperas du etaj kvadratoj en la loko de unu ritma daŭro, ĉiu daŭras duonan daŭron\footnote{Ĉe unu muzikaĵo en la libro, la kanto «Amarilio mia bela», estas kvaronigo.}. La utilo de tiu metodo estas duobla: kaj simpligo por junaj muzikistoj aŭ tiuj, kiuj ne havas sperto en legado de muziknotoj (tio estas pli konkreta, malpli abstrakta), kaj vida alirebligo de la ritma strukturo (oni povas ‘vidi’ la ripetiĝantajn ŝablonojn facile). La malutilo estas malfacileco signifante grandajn malsamecojn je daŭro (oni devas uzi multajn \enlinianotacio{\z}) kaj triolojn.

\hl{Ripitoj} estas signifitaj per \enlinianotacio{\r{0}{1}} kaj \enlinianotacio{\r{1}{0}} (kp.\ \symbolglyph{𝄆} kaj \symbolglyph{𝄇} en la kutima notskribo). Kiam la du ripetojn estas malsamaj, la malsameco estas signifita per numerita \symbolglyph{⌜}.

Ĉu vi volas certi, ke vi komprenas korekte? Jen la komenco de la kanto «\hl{Feliĉan naskiĝtagon}» (Happy Birthday to You). Provu kaj vidu, ĉu ĝi sonas bone?

\begin{samepage}
	\begin{center}
		\plupluinterpremu
		\takto{}{\s\s\s\s\s\s}{\sx}
		\takto{}{\s\s\s\s\c\c}{\x}\par
		\takto{}{\d\z\c\z\f\z}{\x}
		\takto{}{\e\z\z\z\c\c}{\x}\par
		\takto{}{\d\z\c\z\g\z}{\x}
		\takto{}{\f\z\z\z\c\c}{\x}
	\end{center}
\end{samepage}



\eosection{Pli da informo kaj kontaktinformo}

\begin{wrapfigure}[4]{r}{1.75cm}\vspace{-\baselineskip}\includegraphics[width=1.5cm]{retejo.png}\end{wrapfigure}
La \hl{retejo} de Puŝiŝit eldonejo estas
\begin{center}
	\url{http://xpr.digitalwords.net/}
\end{center}
Estas QR-kodo dekstre, por saĝtelefonoj. Ĉe la paĝo de Muzikoloroj en la retejo estas:
\begin{compactitem}
	\item Pli da informo, inkluzive ligiloj por ekstera legado kaj sonregistraĵoj.
	\item PDF-dosiero: por legi per komputilo kaj por presi.
	\item Son-dosieroj, per kiuj vi povus aŭdi ĉiujn muzikaĵojn de la libro. Vere, la ludado ne estas homa, sed aŭtomata-komputila (MIDI); tamen, ĝi povas helpi. La dosieroj estas produktitaj baziĝante sur la notoj de la libro «Ekukulele» (vidu en la retejo de Puŝiŝit), sekve eblas, ke estas malsamaĵetoj.
	\item Fontkodo kaj teĥnika informo pri la liberaj, malfermkodaj iloj, kiujn mi uzis kreinte la libron.
\end{compactitem}

En la retejo vi ankaŭ povas trovi \hl{aliajn librtojn kaj librojn}, de diversaj temoj, inkluzive du aliaj, kiuj havas muzikaĵoj:
\begin{compactitem}
	\item Ekukulele: muzikaĵoj por ukulelo.
	\item Okarineto: facilaj muzikaĵoj por okarino.
\end{compactitem}

Ĉion en ĉi tiu libro, kiun mi mem verkis, mi \hl{liberigas} kiel CC BY: bonvolu kopii, aliigi, disdoni kaj uzi ĝin laŭvole, sed adekvate atribuu.

Pri ĉiu afero, ne hezitu \hl{kontakti} min per retpoŝto ({\url{muzikoloroj}\symbolglyph{❀}\url{digitalwords.net}}) aŭ per telefono (\mbox{+972-2-6419913}). Specife, mi ĝojus aŭdi tion, kiu vi volas diri al mi pri la libro, kaj mi ĝojus ricevi proponojn, plibonigojn, kaj ĝustigojn. Ĉu vi volas sonregistri vin ludante kaj sendi la sonregistraĵon al mi? Mi ĝojegos! \symbolglyph{☺}

\vspace{\baselineskip}
~\hfill
\begin{minipage}{3cm}
	Júda Ronén\\
	Jerusalemo 2016\\
\end{minipage}
