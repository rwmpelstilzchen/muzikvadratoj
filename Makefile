okarino:
	#$(MAKE) -C muzikaĵoj
	./enhavo.py > enhavo.tex
	./muzikaĵoj.py > muzikaĵoj.tex
	latexmk -xelatex -file-line-error okarino

ocarinopvc:
	latexmk -silent -pvc -file-line-error okarino

koloroj:
	#$(MAKE) -C muzikaĵoj
	./enhavo.py > enhavo.tex
	./muzikaĵoj.py > muzikaĵoj.tex
	latexmk -xelatex -file-line-error koloroj

kolorojpvc:
	latexmk -silent -pvc -file-line-error koloroj

clean:
	-rm *.aux *.bbl *.blg *.log *.toc *.url *.cut *.bib *.run.xml *.bst *.bcf *.fls *.fdb_latexmk *.out *.dvi *.idx *.ilg *.ind *.ent

distclean: clean
	-rm *.pdf
